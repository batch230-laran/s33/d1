console.log("Hello World");


// >> SYNCHRONOUS PROOF

// JavaScript is by default synchronous, meaning that it only executes on statement at a time.

/*

console.log("Hello World p.2");
conosle.log("Hello again");
console.log("Good bye");

*/

// Code blocking - waiting for the specific statement to finish before executing the next statement

/*

for(let i=0; i<=1500; i++) {
	console.log(i);
}

console.log("Hello again");

*/

// Asynchronous means that we can proceed to execute other statement, while time consuming  code is running in the background




// GETTING ALL POSTS

fetch("http://jsonplaceholder.typicode.com/posts")
.then(res => console.log(res.json()));


// The Fetch API that allows us to asynchronously request for a resourse (data).
// "fetch()" method in javaScript is used to request to the server and load information on the webpages
// Syntax:
	// fetch("apiURL")

// The ".then()" method captures the response object and returs a promise which will be "fulfilled" or "rejected"

// .json() identifies a network response and converts it directly to JavaScrcipt object


fetch("http://jsonplaceholder.typicode.com/posts")
// .then(res => console.log(res))

.then(res => console.log(res.status))


fetch("http://jsonplaceholder.typicode.com/posts")
.then(res => res.json()) // returns a response that is converted into a Javacript Object
.then(response => console.log(response)); // display the fulfilled response from the previous .then()


/*

// Translating arrow function to TRADITIONAL FUNCTION
fetch("https://jsonplaceholder.typicode.com/posts")
.then(function(res){ 
	return res.json()
})
.then(function(response){ 
	return console.log(response)
})

*/



// GETTING SPECIFIC POST
// ":id" is a wildcard where you can put any value it then creates a link between "id" parameter in the URL and value provided in the URL

/*
fetch("http://jsonplaceholder.typicode.com/posts/1")
.then(res => res.json()) 
.then(response => console.log(response))
*/

// The async and await keyword to achieve asynchronous code.



async function fetchData(){
	let result = await(fetch("http://jsonplaceholder.typicode.com/posts"))
	console.log(result);

	let json = await result.json();
	console.log(json);

	console.log("Hello World");
}

fetchData();





// GET A SPECIFIC DOCUMENT

// GET, POST, PUT, PATCH, DELETE




fetch("http://jsonplaceholder.typicode.com/posts/1")
.then(res => res.json()) 
.then(response => console.log(response))




// POST inserting a document/field

/*
Syntax:
	fetch("apiURL", {options})
	.then((response) => {})
	.then((response)=> {})

*/


fetch("https://jsonplaceholder.typicode.com/posts", 
	{
		method: "POST",
		headers: {
			"Content-Type" : "application/json"
		},
		body:JSON.stringify({
			title: "New Post",
			body: "Hello World",
			userId: 1
		})
	})
	.then(response => response.json())
	.then(json => console.log(json))




fetch("https://jsonplaceholder.typicode.com/posts/1", 
	{
		method: "PUT",
		headers: {
			"Content-Type" : "application/json"
		},
		body:JSON.stringify({
			title: "updated post",
			body: "Hello again",
			userId: 1
		})
	})
	.then(response => response.json())
	.then(json => console.log(json))




// PATCH - updating a specific field of a document
// PUT vs PATCH
	// PATCH is used to upadte single/several properties
	// PUT is used to update the whole document/object


fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "PATCH",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		title: "Corrected post title"
	})
})
.then(response => response.json())
.then(json => console.log(json))



// DELETE deleting a document



fetch("https://jsonplaceholder.typicode.com/posts/1",
	{
		method: "DELETE"
	}).then(response => response.json())
.then(json => console.log(json))
